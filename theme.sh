source ~/${TGBRC_PATH}/sgr.sh

#==============================================================================#
# Color Theme
#------------------------------------------------------------------------------#
defsucc="\[${bld}${grn}\]"
deffail="\[${bld}${red}\]"
defroot="\[${bld}${cyn}\]"
gitbrfg="\[${blk}\]"
gitbrbg="\[$bg_cyn\]"
gitpath="\[${bld}${blu}\]"
gitprms="\[${fnt}${wht}\]"
gitprmf="\[${fnt}${red}\]"
sshtext="\[${ylw}\]"
sshback="\[${bg_blk}\]"
pvetext="\[${bld}$(color_rgb 255 216 73)\]"
pveback="\[$(bgcolor_rgb 55 114 164)\]"
rsttext="\[${rst}\]"
#==============================================================================#


__def_prompt() {
  git rev-parse --git-dir &>/dev/null && return
	RETVAL=$1
	ps1clr=$defsucc
	if [[ $EUID -eq 0 ]]; then ps1clr=$defroot; fi
	if [[ $RETVAL > 0 ]]; then ps1clr=$deffail; fi
	if [[ $EUID -eq 0 ]]; then
		echo -ne "$ps1clr\u@\h:\w\$ $rsttext"
	else
		echo -ne "$ps1clr[\W]\$ $rsttext"
	fi
}

__git_prompt() {
	git rev-parse --git-dir &>/dev/null || return
	branch=$(git rev-parse --abbrev-ref HEAD)
	gitroot=$(git rev-parse --show-toplevel | sed "s/[^\/]*$//" | sed "s/^\(.\):/\/\L\1/")
	PWD=$(pwd)
	RETVAL=$1
	if [[ $RETVAL > 0 ]]; then ps1clr=$gitprmf; else ps1clr=$gitprms; fi
	echo -n "$rsttext$gitbrfg$gitbrbg{$branch}$rsttext$gitpath:${PWD/$gitroot/}$rsttext$ps1clr > git $rsttext"
}

__ssh_prompt() {
	if [[ -n "$SSH_CLIENT" ]]; then
		echo -ne "$sshtext$sshback(\u@\h)$rsttext"
	fi
}

__pve_prompt() {
	if [[ "$VIRTUAL_ENV" != "" ]]; then
		echo -ne "$pvetext$pveback($(basename $VIRTUAL_ENV))$rsttext"
	fi
}

__build_prompt() {
	local RETVAL=$?
	export PS1="$(__ssh_prompt)$(__pve_prompt)$(__git_prompt $RETVAL)$(__def_prompt $RETVAL)"
}

PROMPT_COMMAND="__build_prompt"
