alias reload='source ~/.bashrc'

alias ll='ls -lAhF --group-directories-first'
alias lx='ls -lAhFX'

alias cp='cp -i'
alias  mv='/bin/mv -i'
alias ~mv="/bin/mv -i"
alias rmt='mv -t ~/.local/share/Trash/files'
alias rmp='/bin/rm'
alias ~rm="/bin/rm"
alias mkdir='mkdir -p -v'

alias ~grep="/bin/grep --color=auto"
alias ~diff="/bin/diff"

alias ~reset="/bin/reset"

alias unmount="umount"

alias cd..="cd .."
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'

alias path='echo -e ${PATH//:/\\n}'
alias libpath='echo -e ${LD_LIBRARY_PATH//:/\\n}'

alias du='du -khc'
alias df='df -khT'

alias wget='wget -c'

alias psg="ps -e | /bin/grep"

alias cal="cal -3"

alias which='type -a'

alias pong="ping 8.8.8.8"
alias atx="chmod a+x"
