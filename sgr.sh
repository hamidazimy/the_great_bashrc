#==============================================================================#
# "Set(Select) Graphics Rendition" commands
#==============================================================================#

rst="\e[0m"

bld="\e[1m"
fnt="\e[2m"
ita="\e[3m"
und="\e[4m"
bln="\e[5m"
ovr="\e[53m"
inv="\e[7m"
hid="\e[8m"
xut="\e[9m"

norm="\e[22m"
ita_="\e[23m"
und_="\e[24m"
bln_="\e[25m"
ovr_="\e[55m"
inv_="\e[27m"
hid_="\e[28m"
xut_="\e[29m"

blk="\e[30m"
red="\e[31m"
grn="\e[32m"
ylw="\e[33m"
blu="\e[34m"
mgn="\e[35m"
cyn="\e[36m"
wht="\e[37m"
def="\e[39m"

blk_="\e[90m"
red_="\e[91m"
grn_="\e[92m"
ylw_="\e[93m"
blu_="\e[94m"
mgn_="\e[95m"
cyn_="\e[96m"
wht_="\e[97m"

bg_blk="\e[40m"
bg_red="\e[41m"
bg_grn="\e[42m"
bg_ylw="\e[43m"
bg_blu="\e[44m"
bg_mgn="\e[45m"
bg_cyn="\e[46m"
bg_wht="\e[47m"
bg_def="\e[49m"

bg_blk_="\e[100m"
bg_red_="\e[101m"
bg_grn_="\e[102m"
bg_ylw_="\e[103m"
bg_blu_="\e[104m"
bg_mgn_="\e[105m"
bg_cyn_="\e[106m"
bg_wht_="\e[107m"


color_rgb() {
  echo -n "\e[38;2;${1};${2};${3}m"
}


bgcolor_rgb() {
  echo -n "\e[48;2;${1};${2};${3}m"
}
