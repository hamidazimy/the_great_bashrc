export     LANG="en_CA.UTF-8"
export LANGUAGE="en_CA.UTF-8"
export LC_CTYPE="en_CA.UTF-8"
export   LC_ALL="en_CA.UTF-8"


source ~/${TGBRC_PATH}/theme.sh
source ~/${TGBRC_PATH}/aliases.sh
source ~/${TGBRC_PATH}/git.sh


#******************************************************************************#
#   Customized settings
#******************************************************************************#
if [ -f ~/.bashrc.custom ]; then
	. ~/.bashrc.custom
fi
#******************************************************************************#
