if [ -f ~/.git-completion.bash ]; then
	source ~/.git-completion.bash
fi

alias glone="git clone" # Clone a repository into a new directory

#******************************************************************************#
#    Git aliases
#------------------------------------------------------------------------------#
set_git_aliases() {
    alias     config="git config"                  # Get and set repository or global options
    alias        add="git add"                     # Add file contents to the index
    alias     branch="git branch"                  # List, create, or delete branches
    alias   checkout="git checkout"                # Switch branches or restore working tree files
    alias      clean="git clean"                   # Remove untracked files from the working tree
    alias     commit="git commit"                  # Record changes to the repository
    alias       diff="git diff"                    # Show changes between commits, commit and working tree, etc
    alias      fetch="git fetch"                   # Download objects and refs from another repository
    alias        log="git log"                     # Show commit logs
    alias      merge="git merge"                   # Join two or more development histories together
    alias  mergetool="git mergetool"               # Run merge conflict resolution tools to resolve merge conflicts
    alias      notes="git notes"                   # Add or inspect object notes
    alias       pull="git pull"                    # Fetch from and integrate with another repository or a local branch
    alias       push="git push"                    # Update remote refs along with associated objects
    alias     rebase="git rebase"                  # Reapply commits on top of another base tip
    alias     remote="git remote"                  # Manage set of tracked repositories
    alias      reset="git reset"                   # Reset current HEAD to the specified state
    alias    restore="git restore"                 # Restore working tree files
    alias     revert="git revert"                  # Revert some existing commits
    alias   shortlog="git shortlog"                # Summarize 'git log' output
    alias      stash="git stash"                   # Stash the changes in a dirty working directory away
    alias     status="git status"                  # Show the working tree status

# git basic commands
    alias          a='git add'
    alias          b='git branch'
    alias          c='git checkout'
    alias          d='git diff'
    alias          f='git fetch --prune'
    alias          l='git log --oneline --graph --decorate'
    alias          n='git commit --verbose --amend'
    alias          r='git remote'
    alias          s='git commit --dry-run --short'
    alias          t='git diff --cached'

# git add and the staging area
    alias          a='git add'
    alias         aa='git add --update'            # mnemonic: "add all"
    alias     update='git add --update'
    alias      stage='git add'
    alias         ap='git add --patch'
    alias      patch='git add --patch'
    alias          p='git diff --cached'           # mnemonic: "patch"
    alias         ps='git diff --cached --stat'    # mnemonic: "patch stat"
    alias       stat='git diff --cached --stat'
    alias    unstage='git reset HEAD'

# commits and history
    alias         ci='git commit --verbose'
    alias         ca='git commit --verbose --all'
    alias      amend='git commit --verbose --amend'
    alias          n='git commit --verbose --amend'
    alias          k='git cherry-pick'
    alias         re='git rebase --interactive'
    alias        pop='git reset --soft HEAD^'
    alias       peek='git log -p --max-count=1'

# git fetch
    alias          f='git fetch'
    alias         pm='git pull'                    # mnemonic: pull merge
    alias         pr='git pull --rebase'           # mnemonic: pull rebase

# git diff
    alias          d='git diff'
    alias         ds='git diff --stat'             # mnemonic: "diff stat"

# git reset
    alias       hard='git reset --hard'
    alias       soft='git reset --soft'
    alias      scrap='git checkout HEAD'
    alias    discard='git reset --hard HEAD'
    alias   rollback='git chackout --'

}

reset_git_aliases() {
    unalias     config &>/dev/null
    unalias        add &>/dev/null
    unalias     branch &>/dev/null
    unalias   checkout &>/dev/null
    unalias      clean &>/dev/null
    unalias     commit &>/dev/null
    unalias       diff &>/dev/null
    unalias      fetch &>/dev/null
    unalias        log &>/dev/null
    unalias      merge &>/dev/null
    unalias  mergetool &>/dev/null
    unalias      notes &>/dev/null
    unalias       pull &>/dev/null
    unalias       push &>/dev/null
    unalias     rebase &>/dev/null
    unalias     remote &>/dev/null
    unalias      reset &>/dev/null
    unalias    restore &>/dev/null
    unalias     revert &>/dev/null
    unalias   shortlog &>/dev/null
    unalias      stash &>/dev/null
    unalias     status &>/dev/null

    unalias          a &>/dev/null
    unalias          b &>/dev/null
    unalias          c &>/dev/null
    unalias          d &>/dev/null
    unalias          f &>/dev/null
    unalias          l &>/dev/null
    unalias          n &>/dev/null
    unalias          r &>/dev/null
    unalias          s &>/dev/null
    unalias          t &>/dev/null

    unalias          a &>/dev/null
    unalias         aa &>/dev/null
    unalias     update &>/dev/null
    unalias      stage &>/dev/null
    unalias         ap &>/dev/null
    unalias      patch &>/dev/null
    unalias          p &>/dev/null
    unalias         ps &>/dev/null
    unalias       stat &>/dev/null
    unalias    unstage &>/dev/null

    unalias         ci &>/dev/null
    unalias         ca &>/dev/null
    unalias      amend &>/dev/null
    unalias          n &>/dev/null
    unalias          k &>/dev/null
    unalias         re &>/dev/null
    unalias        pop &>/dev/null
    unalias       peek &>/dev/null

    unalias          f &>/dev/null
    unalias         pm &>/dev/null
    unalias         pr &>/dev/null

    unalias          d &>/dev/null
    unalias         ds &>/dev/null

    unalias       hard &>/dev/null
    unalias       soft &>/dev/null
    unalias      scrap &>/dev/null
    unalias    discard &>/dev/null
    unalias   rollback &>/dev/null
}
#******************************************************************************#

__git() {
    git rev-parse --git-dir &>/dev/null && set_git_aliases || reset_git_aliases
}

PROMPT_COMMAND="${PROMPT_COMMAND};__git"
