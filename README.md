# The Great .bashrc

Installation:
-------------

~~~~
git clone https://gitlab.com/hamidazimy/the_great_bashrc.git ~/.the_great_bashrc

cat <<EOF >>~/.bashrc

#==============================================================================#
# THE GREAT BASHRC!!!
#==============================================================================#
export TGBRC_PATH=".the_great_bashrc"
if [ -f ~/\${TGBRC_PATH}/the_great_bashrc.sh ]; then
    . ~/\${TGBRC_PATH}/the_great_bashrc.sh
fi
#==============================================================================#
EOF

source ~/.bashrc
~~~~

##### No Git? No Problem!
~~~~
wget https://gitlab.com/hamidazimy/the_great_bashrc/-/archive/master/the_great_bashrc-master.tar.gz -O - | tar -xz -C ~ && mv ~/the_great_bashrc-master ~/.the_great_bashrc

cat <<EOF >>~/.bashrc

#==============================================================================#
# THE GREAT BASHRC!!!
#==============================================================================#
export TGBRC_PATH=".the_great_bashrc"
if [ -f ~/\${TGBRC_PATH}/the_great_bashrc.sh ]; then
    . ~/\${TGBRC_PATH}/the_great_bashrc.sh
fi
#==============================================================================#
EOF

source ~/.bashrc
~~~~
